package com.example.dog.fighter;

import com.example.dog.fighter.controller.DogController;
import com.example.dog.fighter.entity.Dog;
import com.example.dog.fighter.repository.DogRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.server.ResponseStatusException;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DogController.class)
public class DogControllerTest {

    @MockBean
    private DogController dogController;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    DogApiClient dogApiClient;


    @MockBean
    DogRepository dogRepository;

    @Test
    void testFindByBreed() throws Exception {

        DogDTO dogDTO = new DogDTO();
        dogDTO.setBreed("hound");

        ResponseEntity<DogDTO> dogs = ResponseEntity.ok(dogDTO);

        given(dogController.findByBreed("hound")).willReturn(dogs);

        mockMvc.perform(get("/find").param("breedName", "hound"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("rating", is(dogDTO.getRating())));
    }

    @Test
    void testFindByBreedFails() throws Exception {

        ImagesDTO imagesDTO = new ImagesDTO();
        imagesDTO.setMessage("failed");
        imagesDTO.setStatus("error");

        given(dogApiClient.getImages("fake breed")).willReturn(imagesDTO);
        given(dogController.findByBreed("fake breed")).willThrow(ResponseStatusException.class);

        mockMvc.perform(get("/find").param("breedName", "fake breed"));
    }

    public static final MediaType APPLICATION_JSON_UTF8 =
            new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(),
                    StandardCharsets.UTF_8);

    @Test
    void testAddToFight() throws Exception {
        Dog dog = new Dog(1, "Igor", "http://vk.com/igorsa", 2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(dog);

        mockMvc.perform(post("/addToFight").content(requestJson).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    void testAddToFightFails() throws Exception {
        Dog dog = new Dog(1, "Igor", "http://vk.com/igorsa", 2);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = ow.writeValueAsString(dog);
        ImagesDTO imagesDTO = new ImagesDTO();
        imagesDTO.setStatus("haha");
        imagesDTO.setMessage("okay");

        when(dogRepository.findDogByImage("Igor")).thenReturn(Optional.empty());
        when(dogController.addToFight(
                new DogDTO(dog.getId(), dog.getBreed(), dog.getImage(), dog.getRating())
        )).thenThrow(ResponseStatusException.class);

        mockMvc.perform(post("/addToFight").content(requestJson).contentType(APPLICATION_JSON_UTF8));
    }


    @Test
    void testGetPair() throws Exception {

        List<DogDTO> dogDTO = new ArrayList<>();

        ResponseEntity<List<DogDTO>> dogs = ResponseEntity.ok(dogDTO);

        given(dogController.getPair()).willReturn(dogs);

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    void testGetPair2() throws Exception {
        DogDTO dog = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJsonDog1 = ow.writeValueAsString(dog);
        String requestJsonDog2 = ow.writeValueAsString(dog2);

        mockMvc.perform(post("/addToFight").content(requestJsonDog1).contentType(APPLICATION_JSON_UTF8));
        mockMvc.perform(post("/addToFight").content(requestJsonDog2).contentType(APPLICATION_JSON_UTF8));


        List<DogDTO> dogDTO = Arrays.asList(dog, dog2);


        ResponseEntity<List<DogDTO>> dogs = ResponseEntity.ok(dogDTO);

        when(dogController.getPair()).thenReturn(dogs);

        mockMvc.perform(get("/getPair"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("*", hasSize(2)));
    }

    @Test
    void testVote() throws Exception {

        ResponseEntity<Boolean> voteResponse = ResponseEntity.ok(true);

        when(dogController.vote(1)).thenReturn(voteResponse);

        mockMvc.perform(get("/vote").param("dogId", String.valueOf(1)))
                .andExpect(status().isOk());
    }

    @Test
    void testRating() throws Exception {
        List<DogDTO> dogs = new ArrayList<>();
    // TODO: create two separate dogDTO objects
        DogDTO dog = new DogDTO(1, "Igor", "http://vk.com/igorlmk", 2);
        DogDTO dog2 = new DogDTO(2, "Harvard", "http://vk.com/igorsam9", 3);
    // Add the two dogs to the list above
        dogs.add(dog);
        dogs.add(dog2);

        ResponseEntity<List<DogDTO>> ratingResponse = ResponseEntity.ok(dogs);

        when(dogController.getRatingList()).thenReturn(ratingResponse);

        mockMvc.perform(get("/rating"))
                .andExpect(status().isOk());
    }
}
