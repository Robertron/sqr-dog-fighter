package com.example.dog.fighter.repository;


import com.example.dog.fighter.entity.Dog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DogRepository extends JpaRepository<Dog,Integer> {

    Optional<Dog> findDogByImage(String image);
}
