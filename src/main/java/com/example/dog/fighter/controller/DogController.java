package com.example.dog.fighter.controller;

import com.example.dog.fighter.DogApiClient;
import com.example.dog.fighter.DogDTO;
import com.example.dog.fighter.entity.Dog;
import com.example.dog.fighter.repository.DogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "/**")
public class DogController {

    @Autowired
    DogApiClient dogApiClient;

    @Autowired
    DogRepository dogRepository;

    @GetMapping("/find")
    public ResponseEntity<DogDTO> findByBreed(@RequestParam(required = true) String breedName) {

        DogDTO response = new DogDTO();
        try {
            if(dogApiClient.getImages(breedName).getStatus().equals("success")) {
                response.setImage(dogApiClient.getImages(breedName).getMessage());
                response.setBreed(breedName);
                response.setRating(0);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Breed not found");
            }
            return ResponseEntity.ok(response);

        } catch(RuntimeException e){
            return ResponseEntity.ok(response);
//            throw new ResponseStatusException(
//                    HttpStatus.NOT_FOUND, "Foo Not Found", e);
        }

    }
    /*

    @GetMapping("/find")
    public ResponseEntity<DogDTO> findByBreed(@RequestParam(required = true) String breedName) {

        DogDTO response = new DogDTO();
        try {
            if(dogApiClient.getImages(breedName).getStatus().equals("success")) {
                response.setImage(dogApiClient.getImages(breedName).getMessage());
                response.setBreed(breedName);
                response.setRating(0);
            }

            return ResponseEntity.ok(response);

        } catch(Exception e){
            return ResponseEntity.ok(response);
//            throw new ResponseStatusException(
//                    HttpStatus.NOT_FOUND, "Foo Not Found", e);
        }

    } */

    @PostMapping("/addToFight")
    public ResponseEntity<Boolean> addToFight(@RequestBody DogDTO dogDTO) {

        Dog dog = new Dog(dogDTO.getId(),dogDTO.getBreed(),dogDTO.getImage(),dogDTO.getRating());
        dog.setRating(0);
        if(dogRepository.findDogByImage(dogDTO.getImage()).isPresent())
            throw new ResponseStatusException(HttpStatus.IM_USED, "dog is already on the fight, try new");
        else {

            dogRepository.save(dog);
            return ResponseEntity.ok(true);
        }
    }

    // look at situation when randomValue1=randomValue2
    @GetMapping("/getPair")
    public ResponseEntity<List<DogDTO>> getPair() {

        List<Dog> dogs = dogRepository.findAll();
        List<DogDTO> dogDTOS = new ArrayList<>();
        int randomValue1 = (int) (Math.random() * dogs.size());
        int randomValue2 = (int) (Math.random() * dogs.size());

        if(dogs.size()==2){
            Dog dog = dogs.get(0);
            DogDTO dogDTO = new DogDTO(dog.getId(),dog.getBreed(),dog.getImage(), dog.getRating());
            dogDTOS.add(dogDTO);
            dog = dogs.get(1);
            dogDTO = new DogDTO(dog.getId(),dog.getBreed(),dog.getImage(), dog.getRating());
            dogDTOS.add(dogDTO);
        }
        if(dogs.size()>2){

            Dog dog = dogs.get(randomValue1);
            DogDTO dogDTO = new DogDTO(dog.getId(),dog.getBreed(),dog.getImage(), dog.getRating());
            dogDTOS.add(dogDTO);
            dog = dogs.get(randomValue2);
            dogDTO = new DogDTO(dog.getId(),dog.getBreed(),dog.getImage(), dog.getRating());
            dogDTOS.add(dogDTO);
        }

        return ResponseEntity.ok(dogDTOS);
    }


    @GetMapping("/vote")
    public ResponseEntity<Boolean> vote(@RequestParam(required = true) Integer dogId) {

        if(dogRepository.findById(dogId).isPresent()){
            Dog dog = dogRepository.findById(dogId).get();
            dog.setRating(dog.getRating()+1);
            dogRepository.save(dog);
            return ResponseEntity.ok(true);
        } else throw new ResponseStatusException(HttpStatus.NOT_FOUND, "there is no dog with this id"+ dogId);
    }

    @GetMapping("/rating")
    public ResponseEntity<List<DogDTO>> getRatingList() {

        List<Dog> dogs = dogRepository.findAll();
        List<DogDTO> dogDTOS = new ArrayList<>();
        for (Dog dog: dogs) {
            DogDTO dogDTO = new DogDTO(dog.getId(),dog.getBreed(),dog.getImage(), dog.getRating());
            dogDTOS.add(dogDTO);
        }

        return ResponseEntity.ok(dogDTOS);
    }

}



