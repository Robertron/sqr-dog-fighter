package com.example.dog.fighter.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class Dog {
    @Id
    @GeneratedValue
    Integer id;

    @Column
    String breed;

    @Column(unique = true)
    String image;

    @Column
    Integer rating;

    public Dog(Integer id, String breed, String image, Integer rating) {
        this.id = id;
        this.breed = breed;
        this.image = image;
        this.rating = rating;
    }

    Dog(){ }
}
